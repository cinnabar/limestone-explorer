use std::path::PathBuf;

use structopt::StructOpt;

pub mod cmd {
    use super::*;

    #[derive(Debug, StructOpt)]
    pub struct Daemon {
        #[structopt(
            help = "Path to the debug info file",
        )]
        pub ident_map: PathBuf,
        #[structopt(short, long, default_value = "1337")]
        pub port: u16,
        #[structopt(short, long)]
        /// File containing list of variable values on the form 'var = value'
        pub vardump: Option<PathBuf>
    }

    #[derive(Debug, StructOpt)]
    pub struct Query {
        #[structopt(short, long, default_value = "1337")]
        pub port: u16,
        #[structopt(name = "ident", help = "Name of the identifier to query")]
        pub query_ident: String,
    }
}

#[derive(Debug, StructOpt)]
pub enum Command {
    Daemon(cmd::Daemon),
    Query(cmd::Query),
}

#[derive(Debug, StructOpt)]
pub struct Opt {
    #[structopt(subcommand)]
    pub cmd: Command
}
