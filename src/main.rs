pub mod daemon;
pub mod options;
pub mod reporting;

use structopt::StructOpt;

use options::{Command, Opt};

use anyhow::Result;

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();

    match opt.cmd {
        Command::Daemon(d) => {
            daemon::run(d).await?;
        }
        Command::Query(q) => {
            let ident = q.query_ident;
            let addr = format!("http://localhost:{}/query/{}", q.port, ident);
            reqwest::get(&addr).await?;
        }
    }
    Ok(())
}
