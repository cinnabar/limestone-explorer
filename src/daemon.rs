use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::collections::HashMap;

use anyhow::{Context, Result};
use limestone::debuginfo::{DebugInfo, FileLocation, IdentDebugInfo};
use limestone::operand::Identifier;
use warp::Filter;
use colored::*;

use crate::options::cmd::Daemon;

fn print_indent(indent: usize, s: &str) {
    for line in s.lines() {
        println!("{}", indent_str(indent, line))
    }
}

pub fn indent_str(amount: usize, s: &str) -> String {
    let mut indent = String::new();
    for _ in 0..amount {
        indent += &"| ";
    }
    format!("{} {}", indent.bright_black(), s)
}

pub fn print_file_location(
    indent: usize,
    location: FileLocation,
) -> Result<()> {
    let relevant_file = location
        .directory
        .map(PathBuf::from)
        .unwrap_or_else(|| PathBuf::new())
        .join(location.filename);

    let file_content = {
        let mut file = File::open(&relevant_file)
            .with_context(|| format!("Failed to open {:?}", relevant_file))?;
        let mut file_content = String::new();
        file.read_to_string(&mut file_content)
            .with_context(|| format!("Failed to read {:?}", relevant_file))?;
        file_content
    };

    let relevant_line = file_content.lines().nth(location.line as usize - 1)
        .unwrap_or("No such line in file");

    let line_number_str = format!("{}: ", location.line);
    let line_number_padding = (0..line_number_str.len()).map(|_| " ").collect::<Vec<_>>().join("");

    let mut pointer_padding = String::new();
    for _ in 0..(location.col.unwrap_or(1) - 1) {
        pointer_padding += " ";
    }
    let pointer = format!(
        "{}{} {}{}",
        line_number_padding,
        ">".bright_cyan(),
        pointer_padding,
        "^".bright_cyan()
    );


    print_indent(indent, &format!(
        "{}{} {}",
        line_number_str.bright_cyan(),
        ">".bright_cyan(),
        relevant_line
    ));
    print_indent(indent, &pointer);
    Ok(())
}

pub fn print_info(
    start: &Identifier,
    map: &DebugInfo,
    indent: usize,
) {
    match map.query(start) {
        Some(infos) => {
            for info in infos {
                match info {
                    IdentDebugInfo::InFile(Some(file)) => {
                        if let Err(e) = print_file_location(indent, file) {
                            print_indent(indent, &format!("{}", e));
                        }
                    }
                    IdentDebugInfo::InFile(None) => {
                        print_indent(indent, &"No file info".red());
                    }
                    IdentDebugInfo::Alias(to) => {
                        let to_str = &format!("{:?}", to);
                        print_indent(
                            indent,
                            &format!("{} {}", "Alias ->".yellow(), to_str)
                        );
                        print_info(&to, map, indent + 1);
                    }
                    IdentDebugInfo::Link(link) => {
                        let message = format!(
                            "{} {} {} {}",
                            "Link ->".white(),
                            &format!("{:?}", link.parent).yellow(),
                            "desc:".white(),
                            &format!("{}", link.description).blue()
                        );
                        print_indent(indent, &message);
                        print_info(&link.parent, map, indent + 1);
                    }
                    IdentDebugInfo::Extra(msg) => {
                        print_indent(indent, &msg)
                    }
                    IdentDebugInfo::Specific(info) => {
                        let message = format!("S: {:?}", info);
                        print_indent(indent, &message)
                    }
                }
            }
        }
        None => {
            print_indent(indent, &format!("No info about {:?}", start))
        }
    }

}

pub fn read_vardump(path: &Path) -> Result<HashMap<String, String>> {
    let content_bytes = std::fs::read(path)
        .context(format!("Failed to read vardump file `{:?}`", path))?;
    let content = String::from_utf8(content_bytes)
        .context(format!("When reading vardump file `{:?}`", path))?;

    Ok(content.lines()
        .filter_map(|line| {
            let mut split = line.splitn(2, "=").map(|s| s.trim());

            if let (Some(name), Some(value)) = (split.next(), split.next()) {
                Some((name.to_string(), value.to_string()))
            }
            else {
                None
            }
        })
        .collect()
    )
}

pub async fn run(opt: Daemon) -> Result<()> {
    // Read the symbol map
    let symbol_map_path = opt .ident_map;

    let symbol_map: DebugInfo = {
        let mut file = File::open(symbol_map_path).context("Failed to open symbol map")?;
        let mut content = String::new();
        file.read_to_string(&mut content)
            .context("Failed to read symbol map")?;
        serde_json::from_str(&content).context("Failed to decode symbol map")?
    };

    let vardump = if let Some(path) = opt.vardump {
        Some(read_vardump(&path)?)
    } else {
        None
    };

    let symbol_query = warp::path!("query" / String).map(move |query: String| {

        println!("{}", &query.blue());

        // Print the value if it is known
        if let Some(vardump) = vardump.as_ref() {
            if let Some(val) = vardump.get(&query) {
                print_indent(1, &format!("{} {}", "val:".bright_green(), val));
            }
        }

        // Check if there are any aliases
        let true_name = Identifier::Str(query.clone());
        print_info(&true_name, &symbol_map, 1);

        format!("ok")
    });

    warp::serve(symbol_query)
        .run(([127, 0, 0, 1], opt.port))
        .await;

    Ok(())
}
