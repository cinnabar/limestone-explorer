use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use limestone::debuginfo::FileLocation;

use codespan_reporting::diagnostic::{Diagnostic, Label};
use codespan_reporting::files::SimpleFiles;
use codespan_reporting::term::{
    self,
    termcolor::{ColorChoice, StandardStream},
};

use anyhow::{anyhow, Context, Result};

pub fn make_label(
    location: FileLocation,
    files: &mut SimpleFiles<String, String>
) -> Result<Label<usize>> {
    let relevant_file = location
        .directory
        .map(PathBuf::from)
        .unwrap_or_else(|| PathBuf::new())
        .join(location.filename);

    let file_content = {
        let mut file = File::open(&relevant_file)
            .with_context(|| format!("Failed to open {:?}", relevant_file))?;
        let mut file_content = String::new();
        file.read_to_string(&mut file_content)
            .with_context(|| format!("Failed to read {:?}", relevant_file))?;
        file_content
    };

    let filename = relevant_file.to_string_lossy().to_string();
    let file_id = files.add(filename, file_content.clone());

    // Generate a span for the file
    let mut line = 1;
    let mut col = 1;
    let mut span = None;
    for (i, bit) in file_content.bytes().enumerate() {
        if line == location.line && location.col.map(|c| c == col).unwrap_or(true) {
            span = Some(i..i);
            break;
        }
        match bit {
            b'\n' => {
                line += 1;
                col = 1;
            }
            _ => col += 1,
        }
    }

    let span = span.ok_or(anyhow!("Line and column not present in the current file"))?;

    Ok(Label::primary(file_id, span))
}

pub fn write_report(
    query: String,
    result: Vec<(Option<FileLocation>, Vec<String>)>
) -> Result<()> {

    // this is ugly but seems to be required because the files struct has lifetime
    // issues
    // let mut filenames = vec![];
    // If we should add a diagnostic location
    let mut files = SimpleFiles::new();

    println!("==========================================================");
    for (location, mut path) in result {
        let mut diagnostic = Diagnostic::note()
            .with_message(query.clone());
        if let Some(location) = location {
            diagnostic = diagnostic
                .with_labels(vec![make_label(location, &mut files)?])
        }
        path.reverse();
        diagnostic = diagnostic.with_notes(path);

        let writer = StandardStream::stderr(ColorChoice::Always);
        let config = codespan_reporting::term::Config::default();

        term::emit(&mut writer.lock(), &config, &files, &diagnostic)?;
    }

    // let diagnostic = diagnostic
    //     .with_notes(notes);


    Ok(())
}
